import torch
from torch.utils.data import DataLoader, Dataset
from torch.autograd import Variable


import cv2
import glob
import numpy as np
from pathlib import Path
from utils._transforms import *
from utils._saliency import *
            
class TrainingDataset(Dataset):
    def __init__(self, data_dir, mode):
        super().__init__()
        """
        'i', 'l' in variable names stand for input and label respectively
        """
        input_format, label_format = '.jpg', '.png'
        raw_i_paths = glob.glob(data_dir + '*' + input_format)
        raw_l_paths = glob.glob(data_dir + '*' + label_format)
        
        # prepare the dataset
        i_paths, l_paths = [], []
        for l_path in raw_l_paths:
            
            name = l_path.split(data_dir)[-1].split(label_format)[0]
            i_path = data_dir + name + input_format
            
            if i_path not in raw_i_paths: continue
            l_paths.append(l_path)
            i_paths.append(i_path)
        
        n_inputs, n_labels = len(np.unique(i_paths)), len(np.unique(l_paths))
        assert n_inputs == n_labels

        self.data_paths = {'input_paths': i_paths, 'label_paths' : l_paths}
        
        # shuffle and split data
        np.random.seed(0)
        ss_ix = np.random.permutation(n_inputs)
        ss_ixs = {'train': ss_ix[:len(ss_ix) * 3 // 5], 
                  'eval': ss_ix[len(ss_ix) * 3 // 5: len(ss_ix) * 4 // 5],
                  'test': ss_ix[len(ss_ix) * 4 // 5:]}
                
        self.dataset = {i: np.array(j)[ss_ixs[mode]] for i, j in self.data_paths.items()}
        
        print('Setting "%s" dataset with %d image-label pairs' %(mode, len(np.unique(self.dataset['input_paths']))))
        
        self.dataset_size = len(self.dataset['input_paths'])
        
        self.transforms = [
            RandomCrop(64),
            Resize(32),
            
        ]
    
    def __getitem__(self, index):
        
        input_path, label_path = self.dataset['input_paths'][index], self.dataset['label_paths'][index]
        input_im, label_im = (read_image(path) for path in [input_path, label_path]) 
        
        for t in self.transforms:                                  
            input_im, label_im = t([input_im, label_im])

        input_im_tensor = Variable(torch.Tensor([input_im]))
        label_im_tensor = Variable(torch.Tensor([label_im]))  
    
        return (input_path, label_path), input_im_tensor, label_im_tensor

    def __len__(self):
        return self.dataset_size
        
class Triplet(Dataset):
    def __init__(self, data_dir, mode):
        super().__init__()
        """
        'i', 'l' in variable names stand for input and label respectively
        """
        
        data_dir = Path(data_dir)
        input_format, label_format = '.jpg', '.png'
        # raw_i_paths = glob.glob(data_dir + '*' + input_format)
        # raw_l_paths = glob.glob(data_dir + '*' + label_format)
        raw_i_paths = [j for i in data_dir.glob('*') if i.is_dir() for j in i.glob('**/*' + input_format)]
        raw_l_paths = [j for i in data_dir.glob('*') if i.is_dir() for j in i.glob('**/*' + label_format)]
        
        # prepare the dataset
        i_paths, l_paths = [], []
        for l_path in raw_l_paths:
            
            name = l_path.name.split(label_format)[0]
            i_path = l_path.parent / (name + input_format)
            
            if Path(i_path) not in raw_i_paths: 
                print(i_path)
                continue
            l_paths.append(l_path)
            i_paths.append(i_path)
        
        n_inputs, n_labels = len(np.unique(i_paths)), len(np.unique(l_paths))
        # random.seed(0)
        # [random.shuffle(j) for j in [i_paths, l_paths] i in range(int(1e4))]
        # assert i_paths[0].name.split('.')[0] == l_paths[0].name.split('.')[0]
        
        assert n_inputs == n_labels

        self.data_paths = {'input_paths': i_paths, 'label_paths' : l_paths}
        
        # shuffle and split data
        np.random.seed(0)
        ss_ix = np.random.permutation(n_inputs)
        ss_ixs = {'train': ss_ix[:len(ss_ix) * 3 // 5], 
                  'eval': ss_ix[len(ss_ix) * 3 // 5: len(ss_ix) * 4 // 5],
                  'test': ss_ix[len(ss_ix) * 4 // 5:]}
                
        self.dataset = {i: np.array(j)[ss_ixs[mode]] for i, j in self.data_paths.items()}
        
        print('Setting "%s" dataset with %d image-label pairs' %(mode, len(np.unique(self.dataset['input_paths']))))
        
        self.dataset_size = len(self.dataset['input_paths'])
        
        self.basic_transforms = [
            RandomCrop(64),
            Resize(32),
        ]
        
        self.aug_transforms = [
            RandomRotation(),
            
        ]
        
        self.sal_transforms = [
            PHOT,  
            strukturtrnsor,
            AC,
            Darker,
            BMS,
            Mcue2
        ]
    
    def __getitem__(self, index):
        
        input_path, label_path = self.dataset['input_paths'][index], self.dataset['label_paths'][index]
        input_im, label_im = (read_image(path) for path in [input_path, label_path]) 
        
        for t in self.basic_transforms:                                  
            input_im, label_im = t([input_im, label_im])

        for t in self.aug_transforms:
            input_gim = t(input_im) 
        
        saliency = False
        if saliency:
            input_sal = []
            for t in self.sal_transforms:
                input_sal.append(t(input_im)) 

        input_im_tensor = Variable(torch.Tensor([input_im]))
        label_im_tensor = Variable(torch.Tensor([label_im]))  
        input_gim_tensor = Variable(torch.Tensor([input_gim])) 
        input_sal_tensor = [Variable(torch.Tensor([i])) for i in input_sal] if saliency else []
                
        return (str(input_path), str(label_path)), input_im_tensor, label_im_tensor, input_gim_tensor, input_sal_tensor

    def __len__(self):
        return self.dataset_size