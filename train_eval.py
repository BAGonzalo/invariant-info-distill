import numpy as np
import torch
import torch.nn as nn

def check_shape(batch):
    if len(batch.shape) > 3 and batch.shape[1] not in [3, 4]:
        batch = batch.squeeze().unsqueeze(1)
#         plot_ims(batch)
    return batch

def train_and_eval_model(device, dataloaders, model, optimizer, loss_fn=None, scheduler=None, epochs=10, verbose=True, max_bs=32):

    modes = ['train', 'eval']
    losses = {i: [] for i in modes}
    ims_log = {i: [] for i in range(epochs)} # {epoch: (im, pred, label)}
    emb_log = {i: [] for i in range(epochs)} # {epoch: (im, pred, label)}
    
    print('\nTraining...')
    for e in range(epochs):
        for mode in modes:
            if mode == 'train': model.train()
            elif mode == 'eval': model.eval()
            else: print('Set "mode" param. to train or eval')

            per_batch_losses = []
            for _, batch in enumerate(dataloaders[mode]): 
                
                paths, ims, labels, trs, sal_maps = batch
                
                # ims, trs = [check_shape(i) for i in [ims, trs]]
                ims, trs = [i.to(device) for i in [ims, trs]]
                # print(ims.shape, trs.shape, sal_maps.shape)
                # predict
                inputs = [ims, trs]
                z = inputs[np.random.randint(2)]
                margX = model(ims)
                margY = model(trs)
                joint = model(z)
                
                # if not loss_fn:
#                 print(margX.shape, margY.shape, joint.shape)
#                 print(type(margX), type(margY), type(joint))
#                 return margX, margY, joint
                
                # option1: loss KL as stated by the paper
                loss_fn = 'KL'
                loss = torch.mean(joint * torch.log(joint / (margX * margY)))
                  # loss = - joint * math.log(joint / (margX + margY))
                
                # option2: cosine loss between the two
                # loss_fn = nn.CosineEmbeddingLoss()
                # loss = loss_fn(margX, margY, torch.Tensor([1.]))
                
                # print(margX, margY, joint, 'Loss: ', loss.item())
                
                per_batch_losses.append(loss.item())
                
                ims_log[e].append((ims, labels)) # ((ims, pred_labels, labels))
                emb_log[e].append((margX, margY))    

                # backpropagate & update weights
                if mode == 'train': 
                    optimizer.zero_grad() 
                    # PyTorch accumulates gradients, which is very handy when you don't have enough 
                    # resources to calculate all the gradients you need in one go.
                    loss.backward()
                    optimizer.step()
                    optimizer.zero_grad() 
                    
            epoch_loss = np.asarray(per_batch_losses).mean()
            losses[mode].append(epoch_loss)
            
            if verbose:
                if mode == 'train': print('epoch: %d | training loss: %.10f | ' %(e, epoch_loss), end='')
                else: print('validation loss: %.10f' %epoch_loss)
                
        if scheduler: scheduler.step()
                
    return ims_log, emb_log, losses