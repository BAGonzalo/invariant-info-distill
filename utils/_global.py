import numpy as np
import torch
import matplotlib.pyplot as plt

def plot_ims(data, alpha=True, cmap=None, ncols=8, nrows=1): # plot_images(12)
    """
    data: expects (1) list of numpy array(s), (2) Variable(s) or (3) Tensor(s)
    """
    if type(data) == np.ndarray: print ("\
    data: expects (1) list of numpy array(s), (2) Variable(s) or (3) Tensor(s)"\
     )

    ncols = min(len(data), ncols)    
    nrows = len(data) // ncols
    if len(data) % ncols != 0: nrows += 1
    
    fig, axes = plt.subplots(nrows=nrows, ncols=ncols, squeeze=False)
    fig.set_size_inches(3*ncols, 3*nrows)
    fig.tight_layout()
        
    for _, ax in enumerate(axes.flat):
        if _ < len(data):
            im = data[_]
            
            if isinstance(im, torch.autograd.Variable): im = im.data
            if isinstance(im, torch.Tensor): 
                if len(im.shape) == 2: im = im.unsqueeze(0)
#                 im = np.reshape(im.numpy(), (im.shape[-2], im.shape[-1], im.shape[-3]))
                im = np.transpose(im.numpy(), (1, 2, 0))
#                 im = (im.transpose(-3, -1)).numpy()
            if len(im.shape) > 2:
                if (im.shape[2] == 4) and alpha: im = im[:,:,[2,1,0,3]]
                if (im.shape[2] == 3): im = im[:,:,[2,1,0]]
                
            im = im.squeeze()
            ax.imshow(im.astype(np.uint8), cmap=cmap)

        ax.set_axis_off()
        ax.title.set_visible(False)
        ax.xaxis.set_ticks([])
        ax.yaxis.set_ticks([])
        for spine in ax.spines.values(): spine.set_visible(False)
        
    plt.subplots_adjust(left=0, hspace=0, wspace=0)
    plt.tight_layout()
    plt.show()