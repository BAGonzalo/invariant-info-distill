import torch
import torch.nn as nn
from torch.utils.data import DataLoader, Dataset
import numpy as np
import sys, os, math
import functools
import glob

from switchnorm import SwitchNorm
print(torch.__version__)

import cv2
import utils
from utils._transforms import *
from utils._global import plot_ims
from datasets import Triplet

# import matplotlib.pyplot as plt
%matplotlib inline

# from model import *
from importlib import reload
reload(utils)

use_cuda = torch.cuda.is_available() and not args.no_cuda
device = torch.device('cuda' if use_cuda else 'cpu')
torch.manual_seed(0)
if use_cuda: torch.cuda.manual_seed(0)

# options
conf_file = {}
input_nc, output_nc = 1, 1
ngf = 64 # of gen filters in first conv layer'
n_downsample_global = 3 # number of downsampling layers in netG
norm = 'switch'
gpu_ids = []
padding_type = 'replicate'
epochs = 1
data_dir = '../data/Magnetic-tile-defect-datasets.-master/MT_Blowhole/Imgs/'
modes = ['train', 'eval', 'test']

# test
datasets = {mode: Triplet(data_dir, mode) for mode in modes}
dataloaders = {mode: DataLoader(datasets[mode], batch_size=8, shuffle=True, num_workers=0, drop_last=False) 
                   for mode in modes} 

model = define_net(conf_file, input_nc, output_nc, ngf, model='encoder', norm='switch',
                       n_downsample_global=n_downsample_global, padding_type=padding_type)
print(model)

optimizer = torch.optim.Adam(model.parameters(), lr=1e-2)
scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=50)
preds, losses = train_and_eval_model(dataloaders, model, optimizer, loss_fn=None, 
                                     scheduler=scheduler, epochs=epochs, verbose=True)
#     eval_model(dataloaders, model, optimizer)
    
plt.plot(losses['train'])
plt.plot(losses['eval'])
plt.tight_layout()
plt.legend(['train loss', 'eval loss'])